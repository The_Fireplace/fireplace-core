package the_fireplace.fireplacecore.config;

import net.minecraft.client.gui.GuiScreen;
import net.minecraftforge.common.config.ConfigElement;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.fml.client.config.GuiConfig;
import the_fireplace.fireplacecore.FireCoreBaseFile;
/**
 * 
 * @author The_Fireplace
 *
 */
public class FireplaceCoreConfigGui extends GuiConfig{

	public FireplaceCoreConfigGui(GuiScreen parentScreen) {
		super(parentScreen, new ConfigElement(FireCoreBaseFile.config.getCategory(Configuration.CATEGORY_GENERAL)).getChildElements(), FireCoreBaseFile.MODID, false,
				false, GuiConfig.getAbridgedConfigPath(FireCoreBaseFile.config.toString()));
	}

}
