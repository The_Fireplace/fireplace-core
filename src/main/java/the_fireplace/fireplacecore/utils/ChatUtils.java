package the_fireplace.fireplacecore.utils;

import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ChatComponentText;

public class ChatUtils {
	/**
	 * Puts a message in the player's chat. Doesn't affect the server.
	 * @param player
	 * 		The player to receive the message
	 * @param message
	 * 		The message to be sent. There is no character limit. It can be split with \n.
	 */
	public static void putMessageInChat(EntityPlayer player, String message) {
		String[] lines = message.split("\n");

		for (String line : lines)
			((ICommandSender) player)
			.addChatMessage(new ChatComponentText(line));
	}
}
