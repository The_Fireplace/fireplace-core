package the_fireplace.fireplacecore;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URLConnection;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.StatCollector;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.common.config.Property;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import the_fireplace.fireplacecore.api.FCAPI;
import the_fireplace.fireplacecore.config.FCCV;
import the_fireplace.fireplacecore.events.FMLEvents;
import the_fireplace.fireplacecore.gui.GuiHandler;
import the_fireplace.fireplacecore.logger.Logger;
import the_fireplace.fireplacecore.math.VersionMath;
import the_fireplace.fireplacecore.utils.ChatUtils;
import the_fireplace.fireplacecore.utils.VersionChecker;
/**
 *
 * @author The_Fireplace
 *
 */
@Mod(modid=FireCoreBaseFile.MODID, name=FireCoreBaseFile.MODNAME, version=FireCoreBaseFile.VERSION, acceptedMinecraftVersions = "1.8", guiFactory = "the_fireplace.fireplacecore.config.FireplaceCoreGuiFactory")
public class FireCoreBaseFile {
	@Instance(value = FireCoreBaseFile.MODID)
	public static FireCoreBaseFile instance;
	public static final String MODID = "fireplacecore";
	public static final String MODNAME = "Fireplace Core";
	public static final String VERSION = "2.3.2.0";

	byte updateNotification;
	static final String downloadURL = "http://goo.gl/1rhqVT";
	public NBTTagCompound update = new NBTTagCompound();
	public GuiHandler guiHandler = new GuiHandler();
	public VersionChecker vc;

	/**
	 * @return
	 * 		The byte form of the update notification setting in the config. 0 is all, 1 is release only, 2 is none
	 */
	public byte getUpdateNotification(){
		return instance.updateNotification;
	}

	public static Configuration config;
	//Config properties
	public static Property UPDATETYPE_PROPERTY;
	public static Property VCSWITCH_PROPERTY;

	public void syncConfig(){
		FCCV.UPDATETYPE = UPDATETYPE_PROPERTY.getString();
		FCCV.VCSWITCH = VCSWITCH_PROPERTY.getBoolean();
		if(config.hasChanged()){
			config.save();
		}
		if((FCCV.UPDATETYPE.toLowerCase()).equals("release")){
			instance.updateNotification = 1;
		}else if((FCCV.UPDATETYPE.toLowerCase()).equals("off") || (FCCV.UPDATETYPE.toLowerCase()).equals("none")){
			instance.updateNotification = 2;
		}else{
			instance.updateNotification = 0;
		}
	}

	@EventHandler
	public void preInit(FMLPreInitializationEvent event) {
		Logger.create(MODID);
		Logger.addToLog(MODID, "Pre-Initializing");
		vc = new VersionChecker();
		FMLCommonHandler.instance().bus().register(new FMLEvents());
		/*if(event.getSide().isClient())
			FMLCommonHandler.instance().bus().register(new KeyHandlerFC());*/
		config = new Configuration(event.getSuggestedConfigurationFile());
		config.load();
		UPDATETYPE_PROPERTY = config.get(Configuration.CATEGORY_GENERAL, FCCV.UPDATETYPE_NAME, FCCV.UPDATETYPE_DEFAULT);
		UPDATETYPE_PROPERTY.comment=StatCollector.translateToLocal("UpdateType.tooltip");
		VCSWITCH_PROPERTY = config.get(Configuration.CATEGORY_GENERAL, FCCV.VCSWITCH_NAME, FCCV.VCSWITCH_DEFAULT);
		VCSWITCH_PROPERTY.comment=StatCollector.translateToLocal("VersionCheckerSwitch.tooltip");
		syncConfig();
		Logger.addToLog(MODID, "Pre-Initialization completed!");
	}

	@EventHandler
	public void init(FMLInitializationEvent event){
		Logger.addToLog(MODID, "Initializing");
		FCAPI.registerModToVersionChecker(update, MODNAME, VERSION, VersionMath.getVersionFor("https://dl.dropboxusercontent.com/s/hwa5d5535luebjy/prerelease.version?dl=0"), VersionMath.getVersionFor("https://dl.dropboxusercontent.com/s/5d9gswpetm66kbg/release.version?dl=0"), downloadURL, MODID);
		NetworkRegistry.INSTANCE.registerGuiHandler(instance, guiHandler);
		Logger.addToLog(MODID, "Initialization completed!");
	}

	public static void onPlayerJoinClient(EntityPlayer player) {
		if(!Loader.isModLoaded("VersionChecker") && FCCV.VCSWITCH){
			ChatUtils.putMessageInChat(player, "<The_Fireplace> Hey, just so you know, I highly recommend downloading Version Checker by Dynious to run alongside Fireplace Core. You can find a link to it on the thread for Fireplace Core, or just look it up on your preferred search engine. To prevent clutter, this message will not be displayed again.");
			Logger.addToLog(MODID, "Player was notified about not having Version Checker");
			VCSWITCH_PROPERTY.setValue(false);//Only runs once, I don't want to spam players, just let them know.
			instance.syncConfig();
		}
	}

	/**
	 * @deprecated as of 2.3.1.0, it is handled automatically by {@link FCAPI#registerModToVersionChecker(NBTTagCompound, String, String, String, String, String, String)}
	 */
	@Deprecated
	public static void sendClientUpdateNotification(EntityPlayer player, String modname, String version, String url){
		if(!Loader.isModLoaded("VersionChecker"))
			sendToPlayer(
					player,
					"A new version of "+modname+" is available!\n=========="
							+ version
							+ "==========\n"
							+ "Download it at " + url + " !");
	}
	/**
	 * @deprecated as of 2.3.1.0, use {@link VersionMath#getVersionFor(String)} to set your releaseVersion and prereleaseVersion
	 */
	@Deprecated
	public static String get_content(URLConnection con) throws IOException {
		String output = "";
		if (con != null) {
			final BufferedReader br = new BufferedReader(new InputStreamReader(
					con.getInputStream()));

			String input;
			StringBuffer buf = new StringBuffer();
			while ((input = br.readLine()) != null) {
				buf.append(input);
			}
			output = buf.toString();
			br.close();
		}
		return output;
	}
	/**
	 * @deprecated as of 2.3.1.0, use {@link FCAPI#registerModToVersionChecker(NBTTagCompound, String, String, String, String, String, String)}
	 */
	@Deprecated
	public void addUpdateInfo(NBTTagCompound updateInfo, String modDisplayName, String oldVersion, String newPreVersion, String newVersion, String updateURL, String modid){
		FCAPI.registerModToVersionChecker(updateInfo, modDisplayName, oldVersion, newPreVersion, newVersion, updateURL, modid);
	}
	/**
	 * @deprecated as of 2.3.1.0, use {@link ChatUtils#putMessageInChat} instead.
	 */
	@Deprecated
	public static void sendToPlayer(EntityPlayer player, String message) {
		ChatUtils.putMessageInChat(player, message);
	}
	/**
	 * @deprecated as of 2.3.1.0, use {@link VersionMath#isHigherVersion(String, String)} instead
	 */
	@Deprecated
	public static boolean isHigherVersion(String currentVersion, String newVersion) {
		return VersionMath.isHigherVersion(currentVersion, newVersion);
	}
}
