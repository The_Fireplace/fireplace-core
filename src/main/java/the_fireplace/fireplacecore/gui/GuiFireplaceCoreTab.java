package the_fireplace.fireplacecore.gui;

import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.util.ResourceLocation;

/**
 * 
 * @author The_Fireplace
 *
 */
public class GuiFireplaceCoreTab extends GuiModTab {
	public GuiFireplaceCoreTab(){
		super();
	}
	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks)
	{
		RenderHelper.enableGUIStandardItemLighting();
		GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
		this.mc.getTextureManager().bindTexture(new ResourceLocation("fireplacecore:textures/gui/modtab_fireplacecore.png"));
		this.drawTexturedModalRect(guiLeft, guiTop, 0, 0, this.xSize, this.ySize);
	}
}
