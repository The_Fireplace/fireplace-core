package the_fireplace.fireplacecore.math;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class VersionMath {
	/**
	 * Checks if the second version is higher than the first
	 *
	 * @param firstVersion
	 *            The first version to compare
	 * @param secondVersion
	 *            The second version to compare
	 * @return true if the second version is higher than the first
	 */
	public static boolean isHigherVersion(String firstVersion, String secondVersion) {
		final int[] _current = splitVersion(firstVersion);
		final int[] _new = splitVersion(secondVersion);

		return (_current[0] < _new[0])
				|| ((_current[0] == _new[0]) && (_current[1] < _new[1]))
				|| ((_current[0] == _new[0]) && (_current[1] == _new[1]) && (_current[2] < _new[2]))
				|| ((_current[0] == _new[0]) && (_current[1] == _new[1]) && (_current[2] == _new[2]) && (_current[3] < _new[3]));
	}
	/**
	 * Splits a version in its number components (Format ".\d+\.\d+\.\d+.*" )
	 *
	 * @param Version
	 *            The version to be splitted (Format is important!
	 * @return The numeric version components as an integer array
	 */
	private static int[] splitVersion(String Version) {
		final String[] tmp = Version.split("\\.");
		final int size = tmp.length;
		final int out[] = new int[size];

		for (int i = 0; i < size; i++) {
			out[i] = Integer.parseInt(tmp[i]);
		}

		return out;
	}

	public static String getVersionFor(String url){
		String output = "";
		URLConnection con;
		try {
			con = new URL(url).openConnection();

			if (con != null) {
				final BufferedReader br = new BufferedReader(new InputStreamReader(
						con.getInputStream()));

				String input;
				StringBuffer buf = new StringBuffer();
				while ((input = br.readLine()) != null) {
					buf.append(input);
				}
				output = buf.toString();
				br.close();
			}
		} catch (MalformedURLException e) {
			System.out.println("MalformedURLException");
			return"0.0.0.0";
		} catch (IOException e) {
			System.out.println("IOException");
			return"0.0.0.0";
		}
		return output;
	}
}
