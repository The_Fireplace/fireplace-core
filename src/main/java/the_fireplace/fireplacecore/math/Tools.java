package the_fireplace.fireplacecore.math;

import java.math.BigDecimal;
import java.math.RoundingMode;
/**
 * 
 * @author The_Fireplace
 *
 */
public class Tools {
	public static double round(double value, int places){
		if(places<0)
			throw new IllegalArgumentException();

		BigDecimal bd = new BigDecimal(value);
		bd = bd.setScale(places, RoundingMode.HALF_UP);
		return bd.doubleValue();
	}

	public static int getIntFromString(String string){
		return Integer.parseInt(string);
	}

	public static String getStringFromInt(int i){
		return String.valueOf(i);
	}

	public static double getJavaVersion(){
		String version = System.getProperty("java.version");
		int pos = version.indexOf('.');
		pos = version.indexOf('.', pos+1);
		return Double.parseDouble(version.substring(0, pos));
	}
}
